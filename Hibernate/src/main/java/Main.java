import entities.Auto;
import entities.Employee;
import tools.EmployeeService;

import java.sql.SQLException;

public class Main {
	public static void main(String[] args) throws SQLException {

		//create employee with 2 auto
		EmployeeService employeeService = new EmployeeService();
		Employee employee = new Employee("Sam",38);
		employeeService.saveEmployee(employee);
		Auto lambo = new Auto("Lambo", "red");
		lambo.setEmployee(employee);
		employee.addAuto(lambo);
		Auto kia = new Auto("Kia", "black");
		kia.setEmployee(employee);
		employee.addAuto(kia);
		employeeService.updateEmployee(employee);
		//rename employee
		employee.setName("Bob");
		employeeService.updateEmployee(employee);
		//delete employee (and related to him auto)
		employeeService.deleteEmployee(employee);
	}
}