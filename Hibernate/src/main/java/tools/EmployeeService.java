package tools;


import entities.Auto;
import entities.Employee;

import java.util.List;

public class EmployeeService {

	private EmployeeCRUD employeeCRUD = new EmployeeCRUD();

	public EmployeeService() {
	}

	public Employee findEmployee(int id) {
		return employeeCRUD.findById(id);
	}

	public void saveEmployee(Employee user) {
		employeeCRUD.save(user);
	}

	public void deleteEmployee(Employee user) {
		employeeCRUD.delete(user);
	}

	public void updateEmployee(Employee user) {
		employeeCRUD.update(user);
	}

	public List<Employee> findAllEmployees() {
		return employeeCRUD.findAll();
	}

	public Auto findAutoById(int id) {
		return employeeCRUD.findAutoById(id);
	}


}