package tools;


import entities.Auto;
import entities.Employee;
import org.hibernate.Session;
import org.hibernate.Transaction;
import java.util.List;

public class EmployeeCRUD {

	public Employee findById(int id) {
		return HibernateSessionFactory.getSessionFactory().openSession().get(Employee.class, id);
	}

	public void save(Employee employee) {
		Session session = HibernateSessionFactory.getSessionFactory().openSession();
		Transaction tx1 = session.beginTransaction();
		session.save(employee);
		tx1.commit();
		session.close();
	}

	public void update(Employee employee) {
		Session session = HibernateSessionFactory.getSessionFactory().openSession();
		Transaction tx1 = session.beginTransaction();
		session.update(employee);
		tx1.commit();
		session.close();
	}

	public void delete(Employee employee) {
		Session session = HibernateSessionFactory.getSessionFactory().openSession();
		Transaction tx1 = session.beginTransaction();
		session.delete(employee);
		tx1.commit();
		session.close();
	}

	public Auto findAutoById(int id) {
		return HibernateSessionFactory.getSessionFactory().openSession().get(Auto.class, id);
	}

	public List<Employee> findAll() {
		List<Employee> employees =
				(List<Employee>)  HibernateSessionFactory
						.getSessionFactory().openSession()
						.createQuery("From Employee").list();
		return employees;
	}
}
