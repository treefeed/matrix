import com.google.auth.oauth2.GoogleCredentials;
import com.google.firebase.FirebaseApp;
import com.google.firebase.FirebaseOptions;
import com.google.firebase.database.*;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeoutException;

public class Main {

	public static void main(String[] args) throws IOException, ExecutionException, InterruptedException, TimeoutException {

		FileInputStream fis = new FileInputStream("C:\\matrix.json");

		FirebaseOptions options = FirebaseOptions.builder()
				.setCredentials(GoogleCredentials.getApplicationDefault())
				.setDatabaseUrl("https://matrix-72068-default-rtdb.europe-west1.firebasedatabase.app")
				.build();

		FirebaseApp.initializeApp(options);
		DatabaseReference ref = FirebaseDatabase.getInstance()
				.getReference("/");
		ValueEventListener listener = new ValueEventListener() {

			public void onDataChange(DataSnapshot dataSnapshot) {
				Object document = dataSnapshot.getValue();
				System.out.println(document);
			}


			public void onCancelled(DatabaseError error) {
				System.out.print("Error: " + error.getMessage());
			}
		};
		//Read User each change
		ref.addValueEventListener(listener);


		DatabaseReference usersRef = ref.child("users");

		//Create Users
		Map<String, User> users = new HashMap<>();
		users.put("alan", new User("June 23, 1912", "Alan Turing"));
		users.put("grace", new User("December 9, 1906", "Grace Hopper"));

		usersRef.setValueAsync(users).get();

		//Update User
		DatabaseReference hopperRef = usersRef.child("grace");
		Map<String, Object> hopperUpdates = new HashMap<>();
		hopperUpdates.put("nickname", "Grace");

		hopperRef.updateChildrenAsync(hopperUpdates).get();

		//Delete User

		DatabaseReference del = usersRef.child("grace");
		del.removeValueAsync().get();
	}
}