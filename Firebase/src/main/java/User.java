import com.google.firebase.database.IgnoreExtraProperties;

@IgnoreExtraProperties
public class User {

	public String birth;
	public String username;

	public User() {
	}

	public User(String birth, String username) {
		this.birth = birth;
		this.username = username;
	}

}