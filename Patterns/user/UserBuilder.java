package meta.user;

import java.sql.Timestamp;

public class UserBuilder {
	private String name = "";
	private String pass = "mint01";
	private MintTestResource res;
	private boolean isAdmin = true;
	private boolean isActive = true;
	private boolean isLocked = false;
	private boolean isPasswordCanExpire = false;
	private boolean isPasswordCanChange = true;

	public UserBuilder(String name) {
		this.name = name + new Timestamp(System.currentTimeMillis()).getTime();
	}

	public UserBuilder setExactName(String name){
		this.name = name;
		return this;
	}

	public boolean isPasswordCanExpire() {
		return isPasswordCanExpire;
	}

	public UserBuilder setPasswordCanExpire(boolean passwordCanExpire) {
		setNonAdmin();
		isPasswordCanExpire = passwordCanExpire;
		return this;
	}

	public boolean isPasswordCanChange() {
		return isPasswordCanChange;
	}

	/**
	 * Because password change prohibition only for Non-Admin users, it set to non-admin automatically
	 *
	 * @param passwordCanChange by default is true
	 * @return builder
	 */
	public UserBuilder setPasswordCanChange(boolean passwordCanChange) {
		setNonAdmin();
		isPasswordCanChange = passwordCanChange;
		return this;
	}

	public boolean isActive() {
		return isActive;
	}

	public UserBuilder setDeactivated() {
		isActive = false;
		return this;
	}

	public boolean isLocked() {
		return isLocked;
	}

	public UserBuilder setLocked() {
		isLocked = true;
		return this;
	}

	public UserBuilder setNonAdmin() {
		isAdmin = false;
		return this;
	}

	public boolean getIsAdmin() {
		return isAdmin;
	}

	public String getName() {
		return name;
	}

	public String getPass() {
		return pass;
	}

	public UserBuilder setPass(final String pass) {
		this.pass = pass;
		return this;
	}

	public MintTestResource getResource() {
		return res;
	}

	public UserBuilder setResource(MintTestResource mintTestResource) {
		this.res = mintTestResource;
		return this;
	}

	public MintTestUser create() {
		return new MintTestUser(this);
	}
}
