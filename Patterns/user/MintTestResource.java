package meta.user;


import mint.api.domain.assistanttree.MintLocation;
import mint.api.domain.assistanttree.MintOrganisation;

import java.sql.Timestamp;

public class MintTestResource {
	private String name = "";
	private String firstName = null;
	private String id = "";
	private MintLocation location = new MintLocation();
	private MintOrganisation organization = new MintOrganisation();
	private String email = "";

	public MintLocation getLocation() {
		return location;
	}

	public MintOrganisation getOrganization() {
		return organization;
	}

	public String getName() {
		return name;
	}

	public String getFirstName() {
		return firstName;
	}

	public MintTestResource setName(String name) {
		this.name = name;
		return this;
	}

	public String getId() {
		return id;
	}

	public String getEmail() {
		return email;
	}

	public static class MRBuilder {
		private final MintTestResource obj;

		public MRBuilder(String id) {
			obj = new MintTestResource();
			obj.id = id + new Timestamp(System.currentTimeMillis()).getTime();
			obj.name = obj.id;
			obj.location.setKey(1L);
			obj.organization.setKey(1L);
		}

		public MRBuilder setLocation(MintLocation location) {
			obj.location = location;
			return this;
		}

		public MRBuilder setOrganization(MintOrganisation organization) {
			obj.organization = organization;
			return this;
		}

		public MRBuilder setName(String name) {
			obj.name = name;
			return this;
		}

		public MRBuilder setFirstName(String firstName){
			obj.firstName = firstName;
			return this;
		}

		public MRBuilder setEmail(String mail) {
			obj.email = mail;
			return this;
		}

		public MintTestResource create() {
			return obj;
		}
	}

}
