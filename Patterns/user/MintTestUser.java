package meta.user;

/**
 * Use UserBuilder for creating User instance. User should be created in system,
 * assign to Permission group (don't use personal permissions)
 */
public class MintTestUser {
	private final String name;
	private String pass;
	private final MintTestResource res;
	private final boolean isAdmin;

	private final boolean isActive;
	private final boolean isLocked;

	private final boolean isPasswordCanExpire;
	private final boolean isPasswordCanChange;

	MintTestUser(UserBuilder builder) {
		this.name = builder.getName();
		this.pass = builder.getPass();
		this.res = builder.getResource();
		this.isAdmin = builder.getIsAdmin();
		this.isActive = builder.isActive();
		this.isLocked = builder.isLocked();
		this.isPasswordCanChange = builder.isPasswordCanChange();
		this.isPasswordCanExpire = builder.isPasswordCanExpire();
	}

	public String getName() {
		return name;
	}

	public String getPass() {
		return pass;
	}

	public MintTestUser setPass(String pass) {
		this.pass = pass;
		return this;
	}

	public MintTestResource getResource() {
		return res;
	}

	public boolean isAdmin() {
		return isAdmin;
	}

	public boolean isLocked() {
		return isLocked;
	}

	public boolean isActive() {
		return isActive;
	}

	public boolean isPasswordCanChange() {
		return isPasswordCanChange;
	}

	public boolean isPasswordCanExpire() {
		return isPasswordCanExpire;
	}
}
