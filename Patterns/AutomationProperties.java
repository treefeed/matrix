package mintangular.web.components.automation;

import mintangular.web.components.Switch;
import mintangular.web.components.TextInput;
import mintangular.web.components.TextInputWithTreePicker;
import mintangular.web.components.WebComponent;
import org.openqa.selenium.By;

import static mintangular.web.components.TextInputWithTreePicker.TreePickerStyle.LIST;
import static mintangular.web.components.TextInputWithTreePicker.TreePickerStyle.TREE;

public class AutomationProperties extends WebComponent<AutomationProperties> {

	AutomationProperties() {
		super();
	}

	public boolean isReadOnly() {
		return !(inpName().isEnabled()
				 && inpDescription().isEnabled()
				 && selOrganisation().isEnabled()
				 && selStatusSet().isEnabled());
	}

	public String getDescription() {
		return inpDescription().getText();
	}

	public AutomationProperties setDescription(String data) {
		inpDescription().setText(data);
		return this;
	}

	private TextInput inpDescription() {
		return new TextInput.Creator(By.xpath("//textarea[@id='automationDescription']")).parentWebComponent(this).create();
	}

	public static class Creator extends WebComponent.Creator<AutomationProperties, AutomationProperties.Creator> {

		public Creator(By xpath) {
			super(xpath);
		}

		@Override
		public AutomationProperties.Creator getBuilder() {
			return this;
		}

		@Override
		public AutomationProperties getObj() {
			return new AutomationProperties();
		}
	}

}
