package mintangular.web.components;

import mintangular.DriverMaster;
import mintangular.web.Component;
import org.openqa.selenium.*;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.testng.TestException;

import java.time.Duration;
import java.util.Collections;
import java.util.List;

import static mintangular.internal.CommonMethods.scrollIntoView;

public abstract class WebComponent<T extends WebComponent<T>> extends Component<T> {

	protected By findByMethod;
	protected String browserType;
	protected WebComponent<?> parentWebComponent;

	/*----------------------- Public events --------------------------*/

	protected WebComponent() {
		super(DriverMaster.getInstance().getDriverInstance());
	}

	public String getBrowserType() {
		return browserType;
	}

	public By getFindByMethod() {
		if (parentWebComponent == null)
			return findByMethod;
		else {
			return By.xpath(parentWebComponent.getFindByMethod().toString().replace("By.xpath: ", "").trim()
							+ findByMethod.toString().replace("By.xpath: .", "").trim()
									.replace("By.xpath: ", "").trim());
		}
	}

	@Override
	public boolean isAvailable() {
		return !getWebElements().isEmpty();
	}

	/**
	 * @return true if element is Displayed
	 */
	public boolean isDisplayed() {
		List<WebElement> elements = getWebElements();
		if (elements.isEmpty()) {
			return false;
		}
		try {
			return elements.get(0).isDisplayed();
		} catch (StaleElementReferenceException | ElementNotVisibleException | NoSuchElementException e) {
			return false;
		}
	}



	/*----------------------- Public methods -------------------------*/

	/**
	 * @return true if element is Enabled. This will return true for everything but disabled input elements.
	 */
	public boolean isEnabled() {
		List<WebElement> elements = getWebElements();
		if (elements.isEmpty()) {
			return false;
		}
		try {
			return elements.get(0).isEnabled();
		} catch (NoSuchElementException e) {
			return false;
		}
	}

	/**
	 * @return true if element is Selected. Only applies to input elements such as checkboxes, options in a select and radio buttons.
	 */
	public boolean isSelected() throws StaleElementReferenceException {
		WebElement element = getWebElement();
		return element.isSelected();
	}

	/**
	 * Clicks the element
	 */
	public void click() throws TimeoutException {
		try {
			Actions builder = new Actions(driver);
			WebElement element = this.getWebElement();
			builder.moveToElement(element).perform();
			this.waitUntilEnabled();
		} catch (NoSuchElementException | TimeoutException e) {
			throw new TestException("Impossible click element " + this.getFindByMethod() + ", because it is hidden or absent");
		}
		WebElement el = getWebElement();
		if (!el.isDisplayed()) {
			scrollIntoView(el, driver);
		}
		try {
			el.click();
		} catch (ElementClickInterceptedException e) {
			driver.executeScript("arguments[0].click()", el);
		}
	}

	/**
	 * Clicks the element twice
	 */
	public void doubleclick() throws TimeoutException {
		Actions builder = new Actions(driver);
		WebElement element = this.getWebElement();
		builder.moveToElement(element).perform();
		try {
			this.waitUntilEnabled();
		} catch (NoSuchElementException | TimeoutException e) {
			throw new TestException("Impossible click element " + this.getFindByMethod() + ", because it is hidden or absent");
		}
		WebElement el = getWebElement();
		if (!el.isDisplayed()) {
			scrollIntoView(el, driver);
		}
		builder.doubleClick(el).perform();
	}


	/**
	 * @return the text from element
	 */
	public String getText() {
		try {
			WebElement el = getWebElement();
			String out = el.getAttribute("innerText");
			String out2 = el.getAttribute("value");
			if (out.equals("")) {
				if (out2 == null) {
					return "";
				}
				return out2.trim();
			}
			return out.trim();
		} catch (WebDriverException e) {
			throw new TestException("Text unavailable\n\n" + e);
		}
	}

	/**
	 * @param propertyName - Name of the CSS property
	 * @return the value of the Element's CSS property
	 */
	public String getCssValue(String propertyName) {
		return getWebElement().getCssValue(propertyName);
	}

	/**
	 * @param attributeName - Name of the Attribute
	 * @return the value of the Element's attribute
	 */
	public String getAttribute(String attributeName) {
		WebElement webElement = this.getWebElement();
		return ngWebDriver.retrieveAsString(webElement, attributeName);
	}

	/**
	 * @return the Location of the current Element
	 */
	public Point getLocation() {
		return getWebElement().getLocation();
	}

	/**
	 * @return Size of the Element
	 */
	public Dimension getSize() {
		return getWebElement().getSize();
	}

	/**
	 * Perform Drag&Drop of current element into provided Element
	 *
	 * @param component - Element to drag into
	 */
	public void dragAndDrop(WebComponent<?> component) {
		WebElement source = this.getWebElement();
		WebElement target = component.getWebElement();
		new Actions(driver).dragAndDrop(source, target).perform();
	}

	/**
	 * Perform Drag&Drop of current element into provided Element
	 *
	 * @param component - Element to drag into
	 */
	public void dragAndDropSpecial(WebComponent<?> component) {
		WebElement source = this.getWebElement();
		WebElement target = component.getWebElement();
		new Actions(driver).moveToElement(source).pause(100L)
				.clickAndHold().pause(100L)
				.moveByOffset(0,10).pause(100L)
				.moveToElement(target).pause(100L)
				.release().perform();
	}

	/**
	 * Perform Drag&Drop of current element into provided location
	 *
	 * @param x - Horizontal offset
	 * @param y - Vertical offset
	 */
	public void dragAndDrop(int x, int y) {
		try {
			WebElement source = this.getWebElement();
			new Actions(driver).dragAndDropBy(source, x, y).build().perform();
		} catch (UnsupportedCommandException e) {
			throw new TestException("Drag and drop is not successful", e);
		}
	}

	public void hoover() {
		try {
			Actions builder = new Actions(driver);
			WebElement element = this.getWebElement();
			builder.moveToElement(element).perform();
			this.waitUntilEnabled();
		} catch (ElementNotInteractableException|NoSuchElementException | TimeoutException e) {
			throw new TestException("Impossible hoover element " + this.getFindByMethod() + ", because it is hidden or absent");
		}
	}

	public String getTooltipText() {
		this.hoover();
		return tooltip().waitUntilVisible().getText();
	}

	private Text tooltip() {//Maybe just use //*[@role='tooltip']//div[0] for this
		return new Text.Creator(By.xpath("//*[@role='tooltip']/div[@class='popover-body']")).create();
	}

	/**
	 * Waits until Element is visible and
	 *
	 * @return this element
	 */
	@SuppressWarnings("unchecked")
	public T waitUntilVisible() {
		waitExpected.until(ExpectedConditions.visibilityOfAllElementsLocatedBy(this.getFindByMethod()));
		return (T) this;
	}

	/**
	 * Waits until Element is invisible and
	 */
	public void waitUntilInvisible() {
		By byMethod = getFindByMethod();
		List<WebElement> elements = driver.findElements(byMethod);
		if (elements.isEmpty()) {
			return;
		}
		try {
			waitExpected.until(ExpectedConditions.invisibilityOfElementLocated(byMethod));
		} catch (TimeoutException ignored) {
		}
	}

	/**
	 * Waits until Element is disappeared
	 *
	 * @return this element
	 */
	@SuppressWarnings("unchecked")
	public T waitUntilDispose() {
		waitExpected.until(ExpectedConditions
				.not(ExpectedConditions
						.presenceOfAllElementsLocatedBy(getFindByMethod())));
		return (T) this;
	}

	/**
	 * Waits until Element is disabled
	 *
	 * @return this element
	 */
	@SuppressWarnings("unchecked")
	public T waitUntilDisabled() {
		waitExpected.withTimeout(Duration.ofSeconds(2L))
				.until(ExpectedConditions
						.not(ExpectedConditions.elementToBeClickable(getWebElement())));
		return (T) this;
	}

	/**
	 * Waits until Element is enabled
	 *
	 * @return this element
	 */
	@SuppressWarnings("unchecked")
	public T waitUntilEnabled() throws TimeoutException {
		waitExpected.withTimeout(Duration.ofSeconds(15L))
				.until(ExpectedConditions.elementToBeClickable(getWebElement()));
		return (T) this;
	}

	/**
	 * Scroll to Element
	 *
	 * @return this element
	 */
	@SuppressWarnings("unchecked")
	public T scrollToElement() {
		WebElement el = getWebElement();
		scrollIntoView(el, driver);
		return (T) this;
	}

	/*----------------------- Methods from WebElement -------------------------*/

	/**
	 * Find the first Element using provided By Method
	 *
	 * @param by - Method to find element
	 * @return this element
	 */
	public WebElement findElement(By by) {
		try {
			return getWebElement().findElement(by);
		} catch (WebDriverException e) {
			this.waitUntilAvailable();
			try {
				return getWebElement().findElement(by);
			} catch (NoSuchElementException e1) {
				return null;
			}
		}
	}

	/**
	 * Find all elements using provided By Method
	 *
	 * @param by - Method to find elements
	 * @return this elements in List
	 */
	public List<WebElement> findElements(By by) {
		try {
			return getWebElement().findElements(by);
		} catch (WebDriverException e) {
			try {
				this.waitUntilAvailable();
			} catch (TimeoutException e1) {
				return Collections.emptyList();
			}
			return getWebElement().findElements(by);
		}
	}

	/**
	 * @return the WebElement of the Component.
	 * You should use (/descendant::) in x-path for Elements, searched from parent
	 */
	public WebElement getWebElement() {
		if (parentWebComponent == null)
			return driver.findElement(findByMethod);
		else {
			try {
				return parentWebComponent.getWebElement().findElement(findByMethod);
			} catch (TimeoutException | NoSuchElementException e1) {
				return null;
			}
		}
	}

	/**
	 * @return the List of WebElements of the Component
	 */
	public List<WebElement> getWebElements() {
		if (parentWebComponent == null)
			return driver.findElements(findByMethod);
		else {
			return parentWebComponent.getWebElement().findElements(findByMethod);
		}
	}


	//----------------------------Builder for WebComponent------------------------------------//
	//https://egalluzzo.blogspot.com/2010/06/using-inheritance-with-fluent.html
	//https://stackoverflow.com/questions/17164375/subclassing-a-java-builder-class

	/**
	 * All non-leaf builders should be generic, all leaf builders should be specific
	 * All non-leaf classes must be abstract, and all leaf classes must be final.
	 *
	 * @param <T> replaced by Element class in leaf classes
	 * @param <B> replaced by Element.Creator class in leaf classes
	 */
	public abstract static class Creator<T extends WebComponent<T>, B extends Creator<T, B>> {

		protected T obj;
		protected B bld;

		protected Creator(By findByMethod) {
			obj = getObj();
			bld = getBuilder();

			obj.findByMethod = findByMethod;
			obj.browserType = DriverMaster.BROWSER_NAME;
		}

		public B parentWebComponent(WebComponent<?> parentWebComponent) {
			obj.parentWebComponent = parentWebComponent;
			return bld;
		}

		public abstract B getBuilder();//used to add constructors without casting to (B)

		public abstract T getObj();//used to add constructors without casting to (B)

		public T create() {
			return obj;
		}

	}
}
