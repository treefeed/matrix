package mintangular.web.components;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;

public class TextInput extends WebComponent<TextInput> {

	TextInput() {
		super();
	}

	public void setText(String text) {
		WebElement el = this
				.scrollToElement()
				.getWebElement();
		el.clear();
		el.sendKeys("0");
		el.sendKeys(Keys.BACK_SPACE);
		el.sendKeys(text);
	}

	/**
	 * Clears the text from the text box
	 */
	public void clear() {
		WebElement element = this.scrollToElement()
				.getWebElement();
		element.clear();
		element.sendKeys("0");
		element.sendKeys(Keys.BACK_SPACE);
	}

	public String getColor() {
		return this.getCssValue("color");
	}

	public static class Creator extends WebComponent.Creator<TextInput, TextInput.Creator> {

		public Creator(By findByMethod) {
			super(findByMethod);
		}

		@Override
		public TextInput.Creator getBuilder() {
			return this;
		}

		@Override
		public TextInput getObj() {
			return new TextInput();
		}
	}
}
