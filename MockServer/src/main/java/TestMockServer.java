import org.mockserver.client.MockServerClient;
import org.mockserver.model.Header;
import org.mockserver.model.HttpForward;

import java.util.concurrent.TimeUnit;

import static org.mockserver.matchers.Times.exactly;
import static org.mockserver.model.HttpForward.forward;
import static org.mockserver.model.HttpRequest.request;
import static org.mockserver.model.HttpResponse.response;
import static org.mockserver.model.StringBody.exact;

public class TestMockServer {
	/**
	 * To mock invalid login
	 */
	private void createExpectationForInvalidAuth() {
		new MockServerClient("127.0.0.1", 1080)
				.when(
						request()
								.withMethod("POST")
								.withPath("/validate")
								.withHeader("\"Content-type\", \"application/json\"")
								.withBody(exact("{username: 'invalid', password: 'invalid'}")),
						exactly(1))
				.respond(
						response()
								.withStatusCode(401)
								.withHeaders(
										new Header("Content-Type", "application/json; charset=utf-8"),
										new Header("Cache-Control", "public, max-age=86400"))
								.withBody("{ message: 'Incorrect username and password combination' }")
								.withDelay(TimeUnit.SECONDS, 1)
						);

	}

	private void createExpectationForForward(){
		new MockServerClient("127.0.0.1", 1080)
				.when(
						request()
								.withMethod("GET")
								.withPath("/index.html"),
						exactly(1))
				.forward(
						forward()
								.withHost("www.mock-server.com")
								.withPort(80)
								.withScheme(HttpForward.Scheme.HTTP)
						);
	}
}